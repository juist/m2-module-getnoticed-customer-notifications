<?php

namespace GetNoticed\CustomerNotifications\Block\Adminhtml\Send\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 *
 * @package GetNoticed\CustomerNotifications\Block\Adminhtml\Send\Edit
 */
class SaveButton
    implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label'          => __('Send notification'),
            'class'          => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order'     => 10
        ];
    }

}