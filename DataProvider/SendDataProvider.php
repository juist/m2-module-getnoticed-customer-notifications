<?php

namespace GetNoticed\CustomerNotifications\DataProvider;

use GetNoticed\CustomerNotifications\Model\ResourceModel\Notification\CollectionFactory;

/**
 * Class SendDataProvider
 *
 * @package GetNoticed\CustomerNotifications\DataProvider
 */
class SendDataProvider
    extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * SendDataProvider constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param string            $name
     * @param string            $primaryFieldName
     * @param string            $requestFieldName
     * @param array             $meta
     * @param array             $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return [];
    }


}