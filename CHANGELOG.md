# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2017-10-30
### Fixed
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : Fix a bug where the GetNoticed_Common module/routes are missing, causing a fatal error

## [1.1.0] - 2017-10-30
### Fixed
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : Remove version from composer.json and fix package name

## [1.0.0] - 2017-10-30
### Added
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : Initial setup of module
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : Frontend KnockoutJS component to read notifications
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : Admin form to send example/test notifications
* [BAM-122](http://jira.getnoticed.nl:8080/browse/BAM-122) : An event dispatch/observe system to send new notifications
