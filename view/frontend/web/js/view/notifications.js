define([
    'jquery',
    'uiComponent',
    'ko',
    'GetNoticed_CustomerNotifications/js/repo/notifications'
], function ($, Component, ko, NotificationsRepo) {
    'use strict';

    return Component.extend({
        notifications: ko.observableArray(),
        notificationsUnread: ko.observableArray(),

        initialize: function () {
            this._super();

            (function (self) {
                // Fill unread notifications
                self.notifications.subscribe(function () {
                    self.notificationsUnread.removeAll();
                    $.each(self.notifications(), function (idx, notification) {
                        if (notification.isRead() !== true) {
                            self.notificationsUnread.push(notification);
                        }
                    });
                });

                // Load notifications from server
                NotificationsRepo.notifications.subscribe(function () {
                    self.notifications(NotificationsRepo.notifications());
                });
                self.reloadNotifications();
                setInterval(self.reloadNotifications, 30 * 1000);
            })(this);
        },

        reloadNotifications: function () {
            NotificationsRepo.loadFromServer();
        },

        getBubbleDivClasses: function (hasNotifications) {
            if (hasNotifications) {
                return {
                    class: 'notifications-bubble has-notifications'
                };
            }

            return {
                class: 'notifications-bubble'
            };
        },

        markAllRead: function () {
            (function (self) {
                self.notificationsUnread.removeAll();
                $.getJSON('/getnoticed/customer_notifications/readall/', [], function (response) {
                    if (response.status !== 'OK') {
                        alert($t('Oops - something went wrong'));
                    }
                });
            })(this);
        }
    });
});