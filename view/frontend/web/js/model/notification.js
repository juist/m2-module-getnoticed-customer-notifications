define([
    'jquery',
    'ko',

    // Libraries
    'GetNoticed_CustomerNotifications/js/lib/jquery/timeago'
], function ($, ko) {
    'use strict';

    return function (data) {
        return {
            notificationId: ko.observable(data.notification_id),
            code: ko.observable(data.code),
            message: ko.observable(data.message),
            url: ko.observable(data.url),
            followUrl: ko.observable('/getnoticed/customer_notifications/follow/notification_id/' + data.notification_id + '/'),
            isRead: ko.observable(data.is_read === '1'),
            createdAt: ko.observable(data.created_at),
            createdAtTimeAgo: ko.observable(null),
            updatedAt: ko.observable(data.updated_at),
            updatedAtTimeAgo: ko.observable(null),

            getRootDivClasses: function () {
                return {
                    class: 'notification notification-' + this.notificationId() + ' notification-' + this.code()
                };
            },

            enableTicks: function () {
                setInterval(this.updateTimeAgoFields, 1000, this);
            },

            updateTimeAgoFields: function (self) {
                self.createdAtTimeAgo($.timeago(self.createdAt()));
                self.updatedAtTimeAgo($.timeago(self.updatedAt()));
            },

            redirectTo: function (url) {
                window.location = url;
            }
        };
    };
});