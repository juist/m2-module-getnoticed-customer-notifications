define([
    'jquery',
    'ko',
    'GetNoticed_CustomerNotifications/js/model/notification'
], function ($, ko, NotificationModelFactory) {
    'use strict';

    return {
        notifications: ko.observableArray(),

        loadFromServer: function () {
            (function (self, fetchUrl) {
                $.getJSON(fetchUrl, [], function (response) {
                    self.notifications.removeAll();
                    if (response.totalRecords > 0) {
                        $.each(response.items, function (idx, data) {
                            var NotificationModel = new NotificationModelFactory(data);
                            NotificationModel.enableTicks();
                            self.notifications.push(NotificationModel);
                        });
                    }
                });
            })(this, '/getnoticed/customer_notifications/all/');
        }
    };
});