<?php

namespace GetNoticed\CustomerNotifications\Controller\Customer\Notifications;

use GetNoticed\CustomerNotifications\Model;
use Magento\Framework;
use Magento\Customer;
use Magento\Store\Model as StoreModel;
use Magento\Store\Api as StoreApi;

/**
 * Class All
 *
 * @package GetNoticed\CustomerNotifications\Controller\Customer\Notifications
 */
class All
    extends Framework\App\Action\Action
{

    /**
     * @var Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var Model\ResourceModel\Notification\Collection
     */
    protected $notificationCollection;

    /**
     * @var Model\ResourceModel\Notification\CollectionFactory
     */
    protected $notificationCollectionFactory;

    /**
     * @var StoreModel\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * All constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session       $customerSession
     */
    public function __construct(
        Framework\App\Action\Context $context,
        Customer\Model\Session $customerSession,
        Model\ResourceModel\Notification\CollectionFactory $notificationCollectionFactory,
        StoreModel\StoreManagerInterface $storeManager
    ) {
        $this->customerSession = $customerSession;
        $this->notificationCollectionFactory = $notificationCollectionFactory;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Return all notifications for current user.
     */
    public function execute()
    {
        /** @var Framework\Controller\Result\Json $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData(
            $this->getNotificationsCollection()->toArray(
                [
                    'notification_id',
                    'code',
                    'message',
                    'url',
                    'is_read',
                    'created_at',
                    'updated_at'
                ]
            )
        );

        return $response;
    }

    /**
     * @return \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification\Collection
     */
    protected function getNotificationsCollection(): Model\ResourceModel\Notification\Collection
    {
        if ($this->notificationCollection === null) {
            $this->notificationCollection = $this->notificationCollectionFactory->create();
            $this->notificationCollection
                ->addFieldToSelect('*')
                ->addFieldToFilter('store_id', $this->getCurrentStore()->getId())
                ->addFieldToFilter('customer_id', $this->customerSession->getCustomerId());
        }

        return $this->notificationCollection;
    }

    /**
     * @return StoreModel\Store|StoreApi\Data\StoreInterface
     */
    protected function getCurrentStore(): StoreApi\Data\StoreInterface
    {
        return $this->storeManager->getStore();
    }

}