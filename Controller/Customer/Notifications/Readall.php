<?php

namespace GetNoticed\CustomerNotifications\Controller\Customer\Notifications;

use Magento\Framework;
use Magento\Customer;
use GetNoticed\CustomerNotifications\Model;

class Readall
    extends Framework\App\Action\Action
{

    /**
     * @var Model\ResourceModel\Notification\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Model\ResourceModel\Notification
     */
    protected $notificationResource;

    /**
     * @var Customer\Model\Session
     */
    protected $customerSession;

    public function __construct(
        Framework\App\Action\Context $context,
        Model\ResourceModel\Notification\CollectionFactory $collectionFactory,
        Model\ResourceModel\Notification $notificationResource,
        Customer\Model\Session $customerSession
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->notificationResource = $notificationResource;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }

    public function execute()
    {
        /** @var Framework\Controller\Result\Json $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);

        try {
            if ($this->customerSession->isLoggedIn() !== true) {
                throw new Framework\Exception\LocalizedException(__('Customer is not logged in.'));
            }

            // @todo Need to rewrite this to a repository instead, because `AbstractModel->delete()` is deprecated in `$collection->walk('delete')`
            $collection = $this->collectionFactory->create();
            $collection
                ->addFieldToFilter('customer_id', $this->customerSession->getId())
                ->addFieldToFilter('is_read', '0');

            foreach ($collection as $notification) {
                /** @var Model\Notification $notification */
                $notification->setIsRead(true);
                $this->notificationResource->save($notification);
            }

            $response->setData(['status' => 'OK']);
        } catch (\Exception $e) {
            $response->setData(['status' => 'error']);
        }

        return $response;
    }

}