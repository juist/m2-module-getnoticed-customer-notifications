<?php

namespace GetNoticed\CustomerNotifications\Controller\Customer\Notifications;

use Magento\Framework;
use Magento\Customer;
use GetNoticed\CustomerNotifications\Model;

/**
 * Class Follow
 *
 * @package GetNoticed\CustomerNotifications\Controller\Customer\Notifications
 */
class Follow
    extends Framework\App\Action\Action
{

    /**
     * @var Model\NotificationFactory
     */
    protected $notificationFactory;

    /**
     * @var Model\ResourceModel\Notification
     */
    protected $notificationResource;

    /**
     * @var Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Follow constructor.
     *
     * @param \Magento\Framework\App\Action\Context                              $context
     * @param \GetNoticed\CustomerNotifications\Model\NotificationFactory        $notificationFactory
     * @param \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification $notificationResource
     * @param \Magento\Customer\Model\Session                                    $customerSession
     */
    public function __construct(
        Framework\App\Action\Context $context,
        Model\NotificationFactory $notificationFactory,
        Model\ResourceModel\Notification $notificationResource,
        Customer\Model\Session $customerSession
    ) {
        $this->notificationFactory = $notificationFactory;
        $this->notificationResource = $notificationResource;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var Framework\Controller\Result\Redirect $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_REDIRECT);

        try {
            /** @var Model\Notification $notification */
            $notification = $this->notificationFactory->create();
            $this->notificationResource->load($notification, $this->getRequest()->getParam('notification_id'));

            if ($notification->getId() === null) {
                throw new Framework\Exception\NoSuchEntityException(__('Notification does not exist.'));
            }

            if ($notification->getCustomer()->getId() !== $this->customerSession->getId()) {
                throw new Framework\Exception\LocalizedException(
                    __('Access denied - notification does not belong to you.')
                );
            }

            $notification->setIsRead(true);
            $this->notificationResource->save($notification);

            $response->setPath($notification->getPath(), $notification->getParams());
        } catch (\Exception $e) {
            $response->setPath('/');
            $this->messageManager->addErrorMessage(
                __('Could not find notification - it might be removed or you do not have access.')
            );
        }

        return $response;
    }

}