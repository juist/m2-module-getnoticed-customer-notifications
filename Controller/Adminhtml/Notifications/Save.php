<?php

namespace GetNoticed\CustomerNotifications\Controller\Adminhtml\Notifications;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

class Save
    extends Action
{

    /**
     * @inheritDoc
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $response */
        $response = $this->resultRedirectFactory->create();
        $response->setPath('*/*/send');

        // Process
        try {
            // Check request
            $data = (array)$this->getRequest()->getPost();

            if (!is_array($data) || count($data) < 1) {
                throw new LocalizedException(__('Invalid request.'));
            }

            // Build params
            $params = [];

            foreach (explode("\n", $data['params']) as $paramLine) {
                if (stripos($paramLine, '=') !== false) {
                    list($paramKey, $paramValue) = explode('=', $paramLine);

                    $paramKey = trim($paramKey);
                    $paramValue = trim($paramValue);

                    $params[$paramKey] = $paramValue;
                }
            }

            $data['params'] = $params;

            // Add notification by event
            $this->_eventManager->dispatch('gn_customer_notify', $data);
            $this->messageManager->addSuccessMessage('Pushed new notification to customer.');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $response;
    }

}