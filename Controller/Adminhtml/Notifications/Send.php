<?php

namespace GetNoticed\CustomerNotifications\Controller\Adminhtml\Notifications;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Send
 *
 * @package GetNoticed\CustomerNotifications\Controller\Adminhtml\Notifications
 */
class Send
    extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_CustomerNotifications::send';

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }

}