<?php

namespace GetNoticed\CustomerNotifications\Setup;

use GetNoticed\CustomerNotifications\Model\ResourceModel\Notification;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl;

class InstallSchema
    implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     * Install notification tables.
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        // Start setup
        /** @var AdapterInterface $adapter */
        $adapter = $setup->getConnection();
        $setup->startSetup();

        // Add notification table
        $this->addNotificationsTable($setup, $adapter);

        // Finish setup
        $setup->endSetup();
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface  $setup
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $adapter
     */
    public function addNotificationsTable(
        SchemaSetupInterface $setup,
        AdapterInterface $adapter
    ) {
        $customerTableName = $setup->getTable('customer_entity');
        $customerIdFieldName = 'entity_id';

        $storeTableName = $setup->getTable('store');
        $storeIdFieldName = 'store_id';

        $table = $adapter
            ->newTable(Notification::TABLE_NAME)
            ->addColumn(
                Notification::ID_FIELD_NAME,
                Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'primary'  => true,
                    'unsigned' => true,
                    'identity' => true,
                    'nullable' => false
                ],
                'Notification Id'
            )
            ->addColumn(
                'customer_id',
                Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false
                ],
                'Customer'
            )
            ->addForeignKey(
                $setup->getFkName(
                    Notification::TABLE_NAME,
                    'customer_id',
                    $customerTableName,
                    $customerIdFieldName
                ),
                'customer_id',
                $customerTableName,
                $customerIdFieldName,
                Ddl\Table::ACTION_CASCADE
            )
            ->addColumn(
                'code',
                Ddl\Table::TYPE_TEXT,
                100,
                [
                    'nullable' => false
                ],
                'Code'
            )
            ->addColumn(
                'message',
                Ddl\Table::TYPE_TEXT,
                1024,
                [
                    'nullable' => false
                ],
                'Message'
            )
            ->addColumn(
                'path',
                Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false
                ],
                'Path'
            )
            ->addColumn(
                'params',
                Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false
                ],
                'Params'
            )
            ->addColumn(
                'store_id',
                Ddl\Table::TYPE_SMALLINT,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false
                ],
                'Store'
            )
            ->addForeignKey(
                $setup->getFkName(
                    Notification::TABLE_NAME,
                    'store_id',
                    $storeTableName,
                    $storeIdFieldName
                ),
                'store_id',
                $storeTableName,
                $storeIdFieldName,
                Ddl\Table::ACTION_CASCADE
            )
            ->addColumn(
                'is_read',
                Ddl\Table::TYPE_INTEGER,
                1,
                [
                    'nullable' => false,
                    'default'  => 0
                ],
                'Is read'
            )
            ->addColumn(
                'created_at',
                Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false
                ],
                'Created at'
            )
            ->addColumn(
                'updated_at',
                Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false
                ],
                'Updated at'
            );
        $adapter->createTable($table);
    }

}