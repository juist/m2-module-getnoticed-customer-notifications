<?php

namespace GetNoticed\CustomerNotifications\Event\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use GetNoticed\CustomerNotifications\Model\NotificationFactory as NotificationFactory;
use GetNoticed\CustomerNotifications\Model\ResourceModel\Notification as NotificationResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\InvalidArgumentException;

/**
 * Class Frontend
 *
 * @package GetNoticed\CustomerNotifications\Event\Observer
 */
class Frontend
    implements ObserverInterface
{

    /**
     * @var NotificationFactory
     */
    protected $notificationFactory;

    /**
     * @var NotificationResource
     */
    protected $notificationResource;

    /**
     * Frontend constructor.
     *
     * @param \GetNoticed\CustomerNotifications\Model\NotificationFactory        $notificationFactory
     * @param \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification $notificationResource
     */
    public function __construct(
        NotificationFactory $notificationFactory,
        NotificationResource $notificationResource
    ) {
        $this->notificationFactory = $notificationFactory;
        $this->notificationResource = $notificationResource;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \GetNoticed\CustomerNotifications\Model\Notification $notification */
        $notification = $this->notificationFactory->create();
        $notification
            ->setCode($observer->getData('code'))
            ->setMessage($observer->getData('message'))
            ->setPath($observer->getData('path'))
            ->setParams($observer->getData('params'));

        // Validate parameters
        if (strlen($notification->getCode()) < 1) {
            throw new InvalidArgumentException('Code is required.');
        }

        if (strlen($notification->getMessage()) < 1) {
            throw new InvalidArgumentException('Message is required.');
        }

        if (strlen($notification->getPath()) < 1) {
            throw new InvalidArgumentException('Path is required.');
        }

        // Customer
        if ($observer->getData('customer_id') !== null) {
            $notification->setCustomerById($observer->getData('customer_id'));
        } elseif ($observer->getData('customer') instanceof \Magento\Customer\Model\Customer) {
            $notification->setCustomer($observer->getData('customer'));
        } else {
            throw new NoSuchEntityException(__('No customer given.'));
        }

        // Store
        if ($observer->getData('store_id') !== null) {
            $notification->setStoreById($observer->getData('store_id'));
        } elseif ($observer->getData('store') instanceof \Magento\Store\Model\Store) {
            $notification->setStore($observer->getData('store'));
        } else {
            throw new NoSuchEntityException(__('No store given.'));
        }

        // Save and add message
        $this->notificationResource->save($notification);
    }

}