<?php

namespace GetNoticed\CustomerNotifications\Api\Data;

use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Interface NotificationInterface
 *
 * @package GetNoticed\CustomerNotifications\Api\Data
 */
interface NotificationInterface
{

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @param string $code
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setCode(string $code): NotificationInterface;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @param string $message
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setMessage(string $message): NotificationInterface;

    /**
     * @return string
     */
    public function getPath(): string;

    /**
     * @param string $path
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setPath(string $path): NotificationInterface;

    /**
     * @return array
     */
    public function getParams(): array;

    /**
     * @param array $params
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setParams(array $params): NotificationInterface;

    /**
     * @return \Magento\Store\Model\Store
     */
    public function getStore(): StoreInterface;

    /**
     * @param \Magento\Store\Model\Store $store
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setStore(StoreInterface $store): NotificationInterface;

    /**
     * @param int $storeId
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setStoreById(int $storeId): NotificationInterface;

    /**
     * Generated url (from path, params and store_id)
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @param \DateTime $createdAt
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setCreatedAt(\DateTime $createdAt): NotificationInterface;

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime;

    /**
     * @param \DateTime $updatedAt
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setUpdatedAt(\DateTime $updatedAt): NotificationInterface;

    /**
     * @return bool
     */
    public function getIsRead(): bool;

    /**
     * @param bool $isRead
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setIsRead(bool $isRead): NotificationInterface;

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer(): CustomerModel;

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setCustomer(CustomerModel $customer): NotificationInterface;

    /**
     * @param int $customerId
     *
     * @return \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface
     */
    public function setCustomerById(int $customerId): NotificationInterface;

}