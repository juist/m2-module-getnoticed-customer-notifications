<?php

namespace GetNoticed\CustomerNotifications\Model\ResourceModel;

use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class Notification
 *
 * @package GetNoticed\CustomerNotifications\Model\ResourceModel
 */
class Notification
    extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Table name
     */
    const TABLE_NAME = 'getnoticed_notifications';

    /**
     * ID field name
     */
    const ID_FIELD_NAME = 'notification_id';

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * Notification constructor.
     *
     * @param \Magento\Framework\Serialize\Serializer\Json      $jsonSerializer
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string                                            $connectionName
     */
    public function __construct(
        Json $jsonSerializer,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    ) {
        $this->jsonSerializer = $jsonSerializer;

        parent::__construct($context, $connectionName);
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::ID_FIELD_NAME);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel|\GetNoticed\CustomerNotifications\Api\Data\NotificationInterface $object
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb|\GetNoticed\CustomerNotifications\Model\ResourceModel\Notification
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        // Save params
        try {
            $object->setData('params', $this->jsonSerializer->serialize($object->getParams()));
        } catch (\Exception $e) {
            $object->setData('params', '[]');
        }

        // Set timestamps
        if ($object->isObjectNew()) {
            $object->setCreatedAt(new \DateTime());
        }

        $object->setUpdatedAt(new \DateTime());

        return parent::_beforeSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel|\GetNoticed\CustomerNotifications\Api\Data\NotificationInterface $object
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb|\GetNoticed\CustomerNotifications\Model\ResourceModel\Notification
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        // Load params
        try {
            $object->setData('params', $this->jsonSerializer->unserialize($object->getData('params')));
        } catch (\Exception $e) {
            $object->setData('params', []);
        }

        return parent::_afterLoad($object);
    }

}