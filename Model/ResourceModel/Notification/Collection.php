<?php

namespace GetNoticed\CustomerNotifications\Model\ResourceModel\Notification;

use GetNoticed\CustomerNotifications\Model\ResourceModel\Notification;

/**
 * Class Collection
 *
 * @package GetNoticed\CustomerNotifications\Model\ResourceModel\Notification
 */
class Collection
    extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * {@inheritdoc}
     */
    protected $_idFieldName = Notification::ID_FIELD_NAME;

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(
            \GetNoticed\CustomerNotifications\Model\Notification::class,
            \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification::class
        );
    }

    /**
     * @inheritDoc
     */
    protected function _afterLoad()
    {
        foreach ($this->getItems() as $notification) {
            /** @var \GetNoticed\CustomerNotifications\Model\Notification $notification */
            $notification->setData('url', $notification->getUrl());
        }

        return parent::_afterLoad();
    }

}