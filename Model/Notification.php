<?php

namespace GetNoticed\CustomerNotifications\Model;

use GetNoticed\CustomerNotifications\Api\Data\NotificationInterface;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\CustomerFactory as CustomerFactory;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * @method \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification getResource()
 * @method \GetNoticed\CustomerNotifications\Model\ResourceModel\Notification\Collection getCollection()
 */
class Notification
    extends \Magento\Framework\Model\AbstractModel
    implements \GetNoticed\CustomerNotifications\Api\Data\NotificationInterface,
               \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * Cache tag used
     *
     * @var string
     */
    const CACHE_TAG = 'getnoticed_customernotifications_notification';

    /**
     * @var string
     */
    protected $_cacheTag = 'getnoticed_customernotifications_notification';

    /**
     * @var string
     */
    protected $_eventPrefix = 'getnoticed_customernotifications_notification';

    /**
     * @var CustomerModel
     */
    protected $customer;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerResource
     */
    protected $customerResource;

    /**
     * @var StoreInterface
     */
    protected $store;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * Notification constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Customer\Model\CustomerFactory                      $customerFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer               $customerResource
     * @param \Magento\Framework\Serialize\Serializer\Json                 $jsonSerializer
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param array                                                        $data
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        CustomerResource $customerResource,
        Json $jsonSerializer,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->customerResource = $customerResource;
        $this->jsonSerializer = $jsonSerializer;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(\GetNoticed\CustomerNotifications\Model\ResourceModel\Notification::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return $this->getData('code');
    }

    /**
     * @inheritDoc
     */
    public function setCode(string $code): NotificationInterface
    {
        return $this->setData('code', $code);
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return $this->getData('message');
    }

    /**
     * @inheritDoc
     */
    public function setMessage(string $message): NotificationInterface
    {
        return $this->setData('message', $message);
    }

    /**
     * @inheritDoc
     */
    public function getPath(): string
    {
        return $this->getData('path');
    }

    /**
     * @inheritDoc
     */
    public function setPath(string $path): NotificationInterface
    {
        return $this->setData('path', $path);
    }

    /**
     * @inheritDoc
     */
    public function getParams(): array
    {
        $params = $this->getData('params');

        if (!is_array($params)) {
            try {
                $params = $this->jsonSerializer->unserialize($params);
            } catch (\Exception $e) {
                $params = [];
            }
        }

        return $params;
    }

    /**
     * @inheritDoc
     */
    public function setParams(array $params): NotificationInterface
    {
        return $this->setData('params', $params);
    }

    /**
     * @inheritDoc
     */
    public function getStore(): StoreInterface
    {
        if ($this->store === null) {
            $this->store = $this->storeManager->getStore($this->getData('store_id'));

            if ($this->store->getId() === null) {
                throw new NoSuchEntityException(__('Store does not exist.'));
            }
        }

        return $this->store;
    }

    /**
     * @inheritDoc
     */
    public function setStore(StoreInterface $store): NotificationInterface
    {
        if ($store->getId() === null) {
            throw new NoSuchEntityException(__('Store does not exist.'));
        }

        $this->store = $store;

        return $this->setData('store_id', $store->getId());
    }

    /**
     * @inheritDoc
     */
    public function setStoreById(int $storeId): NotificationInterface
    {
        return $this->setStore($this->storeManager->getStore($storeId));
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return $this->getStore()->getUrl($this->getPath(), $this->getParams());
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime($this->getData('created_at'));
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(\DateTime $createdAt): NotificationInterface
    {
        return $this->setData('created_at', $createdAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): \DateTime
    {
        return new \DateTime($this->getData('updated_at'));
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(\DateTime $updatedAt): NotificationInterface
    {
        return $this->setData('updated_at', $updatedAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritDoc
     */
    public function getIsRead(): bool
    {
        return !!$this->getData('is_read');
    }

    /**
     * @inheritDoc
     */
    public function setIsRead(bool $isRead): NotificationInterface
    {
        return $this->setData('is_read', $isRead ? '1' : '0');
    }

    /**
     * @inheritDoc
     */
    public function getCustomer(): CustomerModel
    {
        if ($this->customer === null) {
            $this->customer = $this->customerFactory->create();
            $this->customerResource->load($this->customer, $this->getData('customer_id'));

            if ($this->customer->getId() === null) {
                throw new NoSuchEntityException(__('Customer not found.'));
            }
        }

        return $this->customer;
    }

    /**
     * @inheritDoc
     */
    public function setCustomer(CustomerModel $customer): NotificationInterface
    {
        if ($customer->getId() === null) {
            throw new NoSuchEntityException(__('Customer does not exist.'));
        }

        $this->customer = $customer;

        return $this->setData('customer_id', $this->customer->getId());
    }

    /**
     * @inheritDoc
     */
    public function setCustomerById(int $customerId): NotificationInterface
    {
        $this->customer = $this->customerFactory->create();
        $this->customerResource->load($this->customer, $customerId);

        if ($this->customer->getId() === null) {
            throw new NoSuchEntityException(__('Customer does not exist.'));
        }

        return $this->setData('customer_id', $this->customer->getId());
    }

}